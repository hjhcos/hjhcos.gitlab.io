function realTimeGraph() {
    let divStyles = {
        position: 'absolute',
        background: 'rgba(255,255,255,0.95)',
        boxShadow: 'rgb(174, 174, 174) 0px 0px 10px',
        borderRadius: '4px',
    };
    let setStyles = (container, styles) => {
        for (const key in styles) {
            container.style[key] = styles[key];
        }
    };
    fetch('https://gw.alipayobjects.com/os/bmw-prod/5a209bb2-ee85-412f-a689-cdb16159a459.json')
        .then((data) => data.json())
        .then((data) =>
            data.filter((d) => ['United States', 'France', 'Germany', 'Austria', 'Japan', 'Sweden'].includes(d.country))
        )
        .then((data) => {
            let line = new Line('real-time-graph', {
                padding: 'auto',
                appendPadding: [8, 10, 0, 10],
                data,
                xField: 'year',
                yField: 'value',
                seriesField: 'country',
                smooth: true,
                lineStyle: ({country}) => {
                    let importantCountries = ['United States', 'France', 'Germany'];
                    let idx = importantCountries.indexOf(country);
                    return {lineWidth: idx !== -1 ? 2 : 1};
                },
                interactions: [{type: 'brush'}],
                tooltip: {
                    follow: true,
                    enterable: true,
                    offset: 18,
                    shared: true,
                    marker: {lineWidth: 0.5, r: 3},
                },
            });
            line.render();
            let createPie = (container, data) => {
                let piePlot = new Pie(container, {
                    data,
                    width: 120,
                    height: 120,
                    appendPadding: 10,
                    autoFit: false,
                    angleField: 'value',
                    colorField: 'type',
                    legend: false,
                    tooltip: false,
                    animation: false,
                    color: line.chart.themeObject.colors10,
                    label: {
                        type: 'inner',
                        offset: '-10%',
                        content: ({percent}) => `${(percent * 100).toFixed(0)}%`,
                    },
                });
                piePlot.render();
            };
            line.update({
                tooltip: {
                    customContent: (value, items) => {
                        let pieData = items.map((item) => ({
                            type: item.name,
                            value: Number(item.value),
                        }));
                        let container = document.createElement('div');
                        let pieContainer = document.createElement('div');
                        setStyles(container, divStyles);
                        createPie(pieContainer, pieData);
                        container.appendChild(pieContainer);
                        return container;
                    },
                },
            });
            // 初始化，默认激活
            let point = line.chart.getXY(last(data.filter((d) => !!d.value)));
            line.chart.showTooltip(point);
        });
}
function IOPVGraph() {
    fetch('https://gw.alipayobjects.com/os/bmw-prod/1d565782-dde4-4bb6-8946-ea6a38ccf184.json')
        .then((res) => res.json())
        .then((data) => {
            let line = new Line('IOPV-graph', {
                data,
                appendPadding: [50, 0, 0, 0],
                padding: 'auto',
                xField: 'Date',
                yField: 'scales',
                annotations: [
                    {
                        type: 'regionFilter',
                        start: ['min', 'median'],
                        end: ['max', '0'],
                        color: '#F4664A',
                    },
                    {
                        type: 'text',
                        position: ['min', 'median'],
                        content: '中位数',
                        offsetY: -4,
                        style: {
                            textBaseline: 'bottom',
                        },
                    },
                    {
                        type: 'line',
                        start: ['min', 'median'],
                        end: ['max', 'median'],
                        style: {
                            stroke: '#F4664A',
                            lineDash: [2, 2],
                        },
                    },
                ],
            });
            line.render();
        });
}
function scaleActionGraph() {
    let data = [
        {
            type: '家具家电',
            sales: 38,
        },
        {
            type: '粮油副食',
            sales: 52,
        },
        {
            type: '生鲜水果',
            sales: 61,
        },
        {
            type: '美容洗护',
            sales: 145,
        },
        {
            type: '母婴用品',
            sales: 48,
        },
        {
            type: '进口食品',
            sales: 38,
        },
        {
            type: '食品饮料',
            sales: 38,
        },
        {
            type: '家庭清洁',
            sales: 38,
        },
    ];
    let columnPlot = new Column('scale-action-graph', {
        data,
        appendPadding: [50, 0, 0, 0],
        xField: 'type',
        yField: 'sales',
        label: {
            // 可手动配置 label 数据标签位置
            position: 'middle', // 'top', 'bottom', 'middle',
            // 配置样式
            style: {
                fill: '#FFFFFF',
                opacity: 0.6,
            },
        },
        xAxis: {
            label: {
                autoHide: true,
                autoRotate: false,
            },
        },
        meta: {
            type: {
                alias: '类别',
            },
            sales: {
                alias: '销售额',
            },
        },
    });
    columnPlot.render();
}
function init() {
    realTimeGraph();
    IOPVGraph();
    scaleActionGraph();
}
init();